package cs518.sample.usecontactcontentprovider

/*
 * Uses the Contacts Content Provider
 * needs the following in the manifest
 *     <uses-permission android:name="android.permission.READ_CONTACTS"/>
 *  following
 *  http://developer.android.com/guide/topics/providers/content-providers.html
 *  http://developer.android.com/guide/topics/providers/contacts-provider.html
 *  http://www.vogella.com/tutorials/AndroidSQLite/article.html#tutorialusecp_example
 *
 *  This is a simple implementation I leave it to you to implement
 *  a CursorAdapter and an AdapterView and and other functionality.
 */
import android.app.Activity
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.ContactsContract.CommonDataKinds.Phone
import android.widget.TextView

class MainActivity : Activity() {

    private val contacts: Cursor?
        get() {
            val uri = ContactsContract.Contacts.CONTENT_URI
            val projection = arrayOf(ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME)

            val selection = (ContactsContract.Contacts.IN_VISIBLE_GROUP + " = '"
                    + "1" + "'")
            val selectionArgs: Array<String>? = null

            val sortOrder = ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC"
            return contentResolver.query(uri, projection, selection, selectionArgs,
                    sortOrder)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val contactView = findViewById(R.id.tv) as TextView
        val cursor = contacts

        contactView.append("Number of Contacts: " + cursor!!.count + " (Showing first 50)\n")

        var count = 50  // limit to first 50 contacts
        while (cursor.moveToNext() && count-- > 0) {
            val displayName = cursor.getString(cursor
                    .getColumnIndex(ContactsContract.Data.DISPLAY_NAME))
            contactView.append("Name: ")
            contactView.append(displayName)
            // get phone numbers for this contact
            val contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
            val phoneNum = getPhoneNum(contactId)


            contactView.append(" Phone1: ")
            contactView.append(phoneNum)
            contactView.append("\n")
        }
        cursor.close()
    }

    private fun getPhoneNum(contactId: String): String {
        var number: String
        val uri = Phone.CONTENT_URI
        val projection: Array<String>? = null
        val selection = (Phone.CONTACT_ID + " = '"
                + contactId + "'")
        val selectionArgs: Array<String>? = null
        val sortOrder: String? = null

        val phones = contentResolver.query(uri, projection, selection, selectionArgs,
                sortOrder)

        // returns false if cursor empty
        if (phones!!.moveToFirst()) {
            number = phones.getString(phones.getColumnIndex(Phone.NUMBER))
            val type = phones.getInt(phones.getColumnIndex(Phone.TYPE))
            when (type) {
                Phone.TYPE_HOME -> number += " home"
                Phone.TYPE_MOBILE -> number += " mobile"
                Phone.TYPE_WORK -> number += " work"
            }
        } else {
            number = "none"
        }
        phones.close()
        return number
    }

}
